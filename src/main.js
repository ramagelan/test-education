import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase/app'

Vue.config.productionTip = false

  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCh1vofM5rqHjGUmDjS_S9z9DRJxSq0Uqo",
    authDomain: "education-19996.firebaseapp.com",
    databaseURL: "https://education-19996.firebaseio.com",
    projectId: "education-19996",
    storageBucket: "education-19996.appspot.com",
    messagingSenderId: "405660134788",
    appId: "1:405660134788:web:1a8f406995b6702bae01d1"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
